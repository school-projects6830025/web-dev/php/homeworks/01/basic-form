<!DOCTYPE html>
<html lang="cs">
  <head>
    <meta charset="utf-8">
    <title>Domácí úkol - data od uživatele</title>
  </head>
  <body>
  
    <h1>Testovací formulář</h1>
    
    <form method="get">
      <label for="jmeno">Jméno a příjmení</label><br />
      <input type="text" id="jmeno" name="jmeno" value="<?php echo htmlspecialchars($_GET['jmeno'] ?? '')?>" /><br />
      
      <label for="poznamka">Poznámka (10-20 znaků)</label><br />
      <input type="text" name="poznamka" id="poznamka" value="<?php echo htmlspecialchars($_GET['poznamka'] ?? '')?>"><br />
      
      <button type="submit">odeslat</button>
    </form>
    
    <h2>Testovací odkaz</h2>
    
    <a href="formular.php?jmeno=Čížek&poznamka=ahd%20d">Testovací odkaz s parametry</a>
    
    <h2>Vyhodnocení</h2>
  
    <?php
      
      if (empty($_GET)) {
        echo 'nebyla odeslána žádná data';
      } else {

          /*
           * TODO:
           *  1. zkontrolujte, jestli je zadání jméno a příjmení (v proměnné $_GET['jmeno']), odstraňte prázdné znaky ze začátku a konce a poté zkontrolujte, jestli obsahuje mezeru
           *  2. zkontrolujte, jestli je zadána poznámka; délka poznámky by měla být mezi 10-20 znaky (koukněte na funkci mb_strlen)
           *  3. připravte si upravenou hodnotu poznámky, která nebude obsahovat vůbec žádné mezery (str_replace)
           *  4. pokud jsou výše uvedené podmínky splněné, vypište hodnoty do následující tabulky (nezapomeňte na htmlspecialchars)
           */

          // TODO 1.
          if (isset($_GET['jmeno'])) {
              $jmeno = trim($_GET['jmeno']);

              $pos = strpos($jmeno, " ");
              if ($pos === false) {
                  echo "Chybi jmeno nebo prijmeni!\n";
                  return 0;
              } else {
                  echo "Mezera byla nalezena na pozici $pos.\n";
              }
          }

          //TODO 2.
          if (isset($_GET['poznamka'])) {
              $len = mb_strlen($_GET['poznamka']);
              if ($len < 10 || $len > 20) {
                  echo "Poznamka by mela byt 10 - 20 znaku dlouha!\n";
                  return 0;
              } else {
                  $poznamka = $_GET['poznamka'];
              }
              
          } else {
              echo "Poznamka nebyla zadana!\n";
              return 0;
          }

          //TODO 3.
          if (isset($_GET['poznamka'])) {
              $tmp = str_replace(" ", "", $_GET['poznamka']);
              $malaPzn = strtolower($tmp);
          }

              var_dump($_GET);

              echo '<table>
                <tr>
                  <th>Jméno a příjmení</th>
                  <td>'.htmlspecialchars($jmeno).'</td>
                </tr>
                <tr>
                  <th>Poznámka</th>
                  <td>'.htmlspecialchars($poznamka).'</td>
                </tr>
                <tr>
                  <th>Poznámka po převedení na malá písmena, bez mezer</th>
                  <td>'.htmlspecialchars($malaPzn).'</td>
                </tr>
              </table>';

      }
    ?>
  </body>
</html>
